Challprop analysis scripts

In order to run them you will need groovy, R, with knitr and probably Rserve. For groovy, have a look [here](https://bitbucket.org/gbi/challprop-experiments). R environment and Rserve are installed simply by `sudo apt-get install r-base r-cran-rserve` on Ubuntu. knitr is an R package, so has to be installed from within R with `install.packages('knitr', dependencies = TRUE)`.
