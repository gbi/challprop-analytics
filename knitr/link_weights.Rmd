# Link weights

```{r environment-linkWeights, echo=FALSE}
rm(list=ls())
fullPath <- Sys.getenv('R_fullPath')
setwd(fullPath)

links.df <- read.table(file = "linkWeights.dat",header=T,sep=',');
library(ggplot2);
```

The number of links between agents in the graph: `r length(unique(links.df$knowsLink))`;
The average degree `r round(length(unique(links.df$knowsLink))/3000,2)`);
The average link weight is `r round(sum(links.df$weight)/length(unique(links.df$knowsLink)),2)`.

```{r deciles,echo=FALSE}
q <- quantile(links.df$weight,c(0.01,0.1,0.2,0.3,0.5,0.7,0.9,0.99));
q

b <- sum(links.df$weight <= 0);
all <- length(links.df$weight);
```
`r b/all` of all links have negative or zero weight. A  density plot of 98% of link weights (i.e. between 21 and 92) shows the distribution:

```{r link_density ,echo=FALSE}
options(warn=-1);
density <-qplot(weight,data=links.df,geom='histogram',xlim=c(-21,92),binwidth=1)+
  xlab('Weight of the knowsLink (among agents)');
density
options(warn=0);
```


