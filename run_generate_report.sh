#/bin/sh

cd ..
WORKDIR=$(pwd)
ANADIR=$WORKDIR/challprop-analytics

cd $ANADIR
#delete the old results directory
rm -r results
#create results directory
mkdir results

experimentNo=$1

trim() {
    local var=$@
    var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
    echo -n "$var"
}

# run knitr script for building an analysis report
cd $ANADIR/results
#Rscript -e 'dataDir<-$(experimentNo);library(knitr);opts_knit$set(upload.fun = image_uri);knit("../knitr/experiment_report_full.Rmd")';
Rscript ../knitr/knitr_commands.R $experimentNo

