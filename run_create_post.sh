#!/bin/sh

cd ..
WORKDIR=$(pwd)
ELNDIR=$WORKDIR/challprop-eln
ANADIR=$WORKDIR/challprop-analytics

#git commit analysis
#cd $ANADIR
#git add -A
#git commit -m "Autocommit before final report generation"
#commitNoTemp=$(git rev-parse HEAD)
#commitNo=$(echo -n $commitNoTemp | tail -c 7)
#sed -i -e "s/{analysis_commitNo}/$commitNo/g" $ANADIR/results/new_post.md

cd $ELNDIR
rake new_post[$1]
cd source/_posts
POST=$(ls -t | head -n 1)
cat $ANADIR/results/new_post.md >> $POST
echo open $POST with the text editor and correct if needed
