package org.globalbraininstitute.challprop.analytics

import com.thinkaurelius.titan.core.TitanFactory
import com.tinkerpop.pipes.util.structures.Table
import java.nio.file.Files

com.tinkerpop.gremlin.groovy.Gremlin.load()

start = System.currentTimeMillis()
println "running analysis basicStatistics.groovy"
workdir = args[0]
experimentNo = args[1]
experimentDir = workdir +"/eln/"+experimentNo;
println "experimentDir is: " + experimentDir

print "Creating an empty Titan graph database.."
g = TitanFactory.open(experimentDir+"/titan")
println ("...Done :" + (new Date()).toString())

print 'Importing a serialized graph into titan...'
g.loadGraphSON(experimentDir + '/graph.graphson')
println ("...Done :" + (new Date()).toString())

def analysisDataTemp = new File(experimentDir + '/analysisDataTemp.dat')
analysisDataTemp << "// basicStatistics.groovy START\n\n"

print "Getting basic statistics + writing to file.."

analysisDataTemp << "vertexes { \n"

def situation = g.V('type','situation').count()
analysisDataTemp << 'situation='+situation+ "\n"

def benefitHolder = g.V('type','benefitHolder').count()
analysisDataTemp << 'benefitHolder='+benefitHolder+ "\n"

def situationInst = g.V('type','situationInst').count()
analysisDataTemp << 'situationInst='+situationInst+ "\n"

def agent = g.V('type','agent').count()
analysisDataTemp << 'agent='+agent+ "\n"

def god = g.V('type','god').count()
analysisDataTemp << 'god='+god+ "\n"

analysisDataTemp << "}\n"

analysisDataTemp << "edges { \n"

def knowsLinks = g.V('type','agent').outE('knows').inV().has('type','agent').back(2).count()
analysisDataTemp << 'knowsLinks='+knowsLinks+ "\n"

def challenged = g.V('type','agent').inE('challenged').count()
analysisDataTemp << 'challenged='+challenged+ "\n"

def interpreted = g.V('type','agent').outE('interpreted').count()
analysisDataTemp << 'interpreted='+interpreted+ "\n"

def buffered = g.V('type','agent').outE('buffered').count()
analysisDataTemp << 'buffered='+buffered+ "\n"

def processed = g.V('type','agent').outE('processed').count()
analysisDataTemp << 'processed='+processed+ "\n"

def reinterpreted = g.V('type','agent').outE('reinterpreted').count()
analysisDataTemp << 'reinterpreted='+reinterpreted+ "\n"

def forgot = g.V('type','agent').outE('forgot').count()
analysisDataTemp << 'forgot='+forgot+ "\n"

analysisDataTemp << "}\n"

println ("...Done :" + (new Date()).toString())

analysisDataTemp << "// basicStatistics.groovy FINISH\n\n"

analysisDataTemp << 'averageBenefit='+g.V('type','agent').benefit.sum()/g.V('type','agent').count()+ "\n"

analysisDataTemp <<"// "


//benefit analysis

print "Performing benefit analysis...."

penalties = new Table();g.V('type','agent').as('agent').outE('penalized').label.as('event').back(1).timestamp.as('timestamp').back(1).inV.value.as('penalty').table(penalties).iterate()
benefits = new Table();g.V('type','agent').as('agent').outE('benefitted').label.as('event').back(1).timestamp.as('timestamp').back(1).inV.value.as('benefit').table(benefits).iterate()

bfile = new FileWriter(experimentDir+ "/benefitsPenalties.dat")
bfile << "agent,timestamp,event,value\n"
penalties.each{row->
	bfile << row.getColumn('agent').toString() +","+
	row.getColumn('timestamp').toString()+","+
	row.getColumn('event').toString()+","+
	row.getColumn('penalty').toString() + "\n"
}
benefits.each{row->
	bfile << row.getColumn('agent').toString() +","+
	row.getColumn('timestamp').toString()+","+
	row.getColumn('event').toString()+","+
	row.getColumn('benefit').toString() + "\n"
}
bfile.close()

println ("...Done :" + (new Date()).toString())
println "Time of analysis (min): "+(System.currentTimeMillis() - start)/60000

