package org.globalbraininstitute.challprop.analytics

import com.thinkaurelius.titan.core.TitanFactory
import com.tinkerpop.pipes.util.structures.Table
import java.nio.file.Files

com.tinkerpop.gremlin.groovy.Gremlin.load()

start = System.currentTimeMillis()
println "running analysis links.groovy"
workdir = args[0]
experimentNo = args[1]
experimentDir = workdir +"/eln/"+experimentNo;
println "experimentDir is: " + experimentDir

print "Openning graph database.."
g = TitanFactory.open(experimentDir+"/titan")
println "...Done"

//link weights analysis
print 'Extracting link weight information to the file ...'

weightFile = new FileWriter(experimentDir+"/linkWeights.dat")
weightFile<<"knowsLink,weight,fromAgent,toAgent\n"

g.V('type','agent').outE('knows').inV.has('type','agent').back(2)\
	.sideEffect{weightFile<<it.toString()+","}\
	.sideEffect{weightFile<<it.weight.toString()+","}\
	.sideEffect{weightFile<<it.outV().next().toString()+","}\
	.sideEffect{weightFile<<it.inV().next().toString()+"\n"}\
	.iterate()


weightFile.close()

println "...Done"
println "Time of analysis (min): "+(System.currentTimeMillis() - start)/60000
