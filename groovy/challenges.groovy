package org.globalbraininstitute.challprop.analytics

import com.thinkaurelius.titan.core.TitanFactory
import com.tinkerpop.pipes.util.structures.Table
import java.nio.file.Files

com.tinkerpop.gremlin.groovy.Gremlin.load()

start = System.currentTimeMillis()
println "running analysis challenges.groovy:"
workdir = args[0]
experimentNo = args[1]
experimentDir = workdir +"/eln/"+experimentNo;
println "experimentDir is: " + experimentDir

print "Openning graph database.."
g = TitanFactory.open(experimentDir+"/titan")
println "...Done"

print "Extracting data about all atomic events in the database ..."
god = g.V('type','god').next()
challengesFile = new FileWriter(experimentDir+'/allChallengedAgents.dat')
challengesFile<<"agent,challenged,originalChallenges,penalized,benefitted,buffered,forgot,interpreted,reinterpreted,processed,sumOfIntensities\n"
g.V('type','agent')\
	.sideEffect{challengesFile<<it.toString() +","}\
	.sideEffect{challengesFile<<it.inE('challenged').count().toString() +","}\
	.sideEffect{challengesFile<<it.inE('challenged').has('source',god.identity).count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('penalized').count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('benefitted').count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('buffered').count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('forgot').count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('interpreted').count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('reinterpreted').count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('processed').count().toString() +","}\
	.sideEffect{challengesFile<<it.outE('interpreted').intensity.sum().toString() +"\n"}\
	.iterate()

challengesFile.close()

println "...Done"
println "Time of analysis (min): "+(System.currentTimeMillis() - start)/60000









