package org.globalbraininstitute.challprop.analytics

import com.thinkaurelius.titan.core.TitanFactory
import com.tinkerpop.pipes.util.structures.Table
import java.nio.file.Files

com.tinkerpop.gremlin.groovy.Gremlin.load()

start = System.currentTimeMillis()
println "running analysis degrees.groovy"
workdir = args[0]
experimentNo = args[1]
experimentDir = workdir +"/eln/"+experimentNo;
println "experimentDir is: " + experimentDir

print "Opening graph database.."
g = TitanFactory.open(experimentDir+"/titan")
println "...Done"

//get degrees
print "Getting degree data from the graph database..."
degreesFile = new FileWriter(experimentDir+'/degrees.dat')
degreesFile<<"agent,outDegree,outWeightSum,inDegree,inWeightSum\n"
g.V('type','agent')\
	.sideEffect{degreesFile<<it.toString() +","}\
	.sideEffect{degreesFile<<it.outE('knows').inV.has('type','agent').count().toString()+","}\
	.sideEffect{degreesFile<<it.outE('knows').inV.has('type','agent').back(2).weight.sum().toString()+","}\
	.sideEffect{degreesFile<<it.inE('knows').inV.has('type','agent').count().toString()+","}\
	.sideEffect{degreesFile<<it.inE('knows').inV.has('type','agent').back(2).weight.sum().toString()+"\n"}\
	.iterate()

degreesFile.close()

println "...Done"
println "Time of analysis (min): "+(System.currentTimeMillis() - start)/60000

