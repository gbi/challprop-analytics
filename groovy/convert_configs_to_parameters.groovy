Map parameters = new ConfigSlurper('config').parse(new File('/media/data/gbi/eln/challprop-experiments/configs/challprop.conf').toURI().toURL())
commitNo = 'a85a0a5d350155c97ffe8139c5b70a0ec78d280f'
File file = new File("../../data/"+commitNo+"/parameters.conf")
file.createNewFile()
file.withWriter{ writer ->
    parameters.writeTo( writer )
    writer.close()
}
