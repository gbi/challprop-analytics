#!/bin/sh

cd ..
WORKDIR=$(pwd)
ANADIR=$WORKDIR/challprop-analytics
EXPDIR=$WORKDIR/challprop-experiments
CODEDIR=$WORKDIR/challprop

experimentNo=$1

if [ $experimentNo = "last" ]; then
  cd $EXPDIR
  experimentNo="$(git rev-parse HEAD)"
fi

#$ANADIR/run_analysis.sh $experimentNo
$ANADIR/run_generate_report.sh $experimentNo

ELNDIR=$WORKDIR/eln/$experimentNo/
mv $ANADIR/results $ELNDIR
