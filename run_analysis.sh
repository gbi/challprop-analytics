#!/bin/sh

# deleting everythig in report directory making space for the new stuff
cd ..
WORKDIR=$(pwd)
ANADIR=$WORKDIR/challprop-analytics
EXPDIR=$WORKDIR/challprop-experiments
CODEDIR=$WORKDIR/challprop

experimentNo=$1

if [ $experimentNo = "last" ]; then
  cd $EXPDIR
  experimentNo="$(git rev-parse HEAD)"
fi

export R_dataDir=$experimentNo

# run analytics code
cd $ANADIR/groovy
groovy basicStatistics.groovy $WORKDIR $experimentNo
groovy challenges.groovy $WORKDIR $experimentNo
groovy degrees.groovy $WORKDIR $experimentNo
groovy links.groovy $WORKDIR $experimentNo

